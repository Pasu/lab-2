from django.shortcuts import render
from lab_2_addon.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Saya adalah salah satu mahasiswa di Fakultas Ilmu Komputer Universitas Indonesia.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)