from django.shortcuts import render
from lab_2.views import landing_page_content,mhs_name
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


# Create your views here.
response = {'author': "Pasu Siahaan"} #TODO Implement yourname
about_me = ["Pasu Siahaan","Ilmu Komputer 2016","1606896174","pasusiahaan820@gmail.com","081398826523","Depok"]
def index(request):
	response['content'] = landing_page_content
	html = 'lab_4/lab_4.html'
	response['message_form'] = Message_Form
	#TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
	response['about_me'] = about_me
	return render(request, html, response)
def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
        message.save()
        html ='lab_4/form_result.html'
        return render(request, html, response)
    else:        
        return HttpResponseRedirect('/lab-4/')

def message_table(request):
        message = Message.objects.all()
        response['message'] = message
        html = 'lab_4/table.html'
        return render(request, html , response)
